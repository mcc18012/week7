import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;
import java.util.*;


@WebServlet(name = "RunPage", urlPatterns={"/Servlet"})
public class RunPage extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String characterName = request.getParameter("cname");
        String playerName = request.getParameter("pname");
        String raceChoice = request.getParameter("race");
        String[] weapon = request.getParameterValues("weapon");
        int charLevel = Integer.parseInt(request.getParameter("level"));

        out.println("<h1>Your Character</h1>");
        out.println("<p>Player Name: " + playerName + "</p><br>");
        out.println("<p>Character Name: " + characterName + "</p>");
        out.println("<p>Race: " + raceChoice + "</p>");
        out.println("<p>Character level: " + charLevel + "</p>");
        out.println("<p>Weapon Proficiencies: </p>");
        out.println("<ul>");
        for (String wep : weapon) {
            out.println("<li>" + wep + "</li>");
        }
        out.print("</ul>");
        out.println("<p>Congratulations! Your character is done!</p>");
        out.println("</body></html>");

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");

        ArrayList array = new ArrayList();
        String cname = request.getParameter("cname");
        String pname = request.getParameter("pname");
        if ((cname == null) || (cname.equals(""))) {
            array.add("Please enter character name.");
        }
        if ((pname == null) || (pname.equals(""))) {
            array.add("Please enter player name.");
        }
        if (array.size() != 0) {
            out.println(array);

        }
    }
}

